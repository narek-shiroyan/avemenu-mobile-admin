/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
import messaging from '@react-native-firebase/messaging';
import { NavigationContainer } from '@react-navigation/native';
import { NativeBaseProvider } from 'native-base';
import React, { useEffect, useState } from 'react';
import 'react-native-gesture-handler';
import Routes from './src/navigation/routes';

const App = props => {
    const [update, setUpdate] = useState(0);
    useEffect(() => {
        //loadStart();
        const unsubscribe = messaging().onMessage(async remoteMessage => {
            //Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
            // console.log(JSON.stringify(remoteMessage));
            //console.log('state', update);
            await setUpdate(update + 1);

            //Alert.alert('ДАША', 'ПОКАЖИ ПОПУ');
            // console.log(remoteMessage);
        });
        return unsubscribe;
    }, [update]);

    // console.log('APP UPDATE', update);
    return (
        <NativeBaseProvider>
            <NavigationContainer>
                <Routes update={update} />
            </NavigationContainer>
        </NativeBaseProvider>
    );
};

export default App;
