import axios from 'axios';
import { HOST, TOKEN } from '../constants/config';

export const signIn = (login, password, push, platform) => {
    return axios({
        method: 'POST',
        url: `${HOST}/staff/signin`,
        headers: {
            'Access-Token': TOKEN,
        },
        data: {
            login,
            password,
            push,
            platform,
        },
    });
};

export const checkSign = (token, push, platform) => {
    return axios({
        method: 'POST',
        url: `${HOST}/staff/check`,
        headers: {
            'Access-Token': TOKEN,
        },
        data: {
            token,
            push,
            platform,
        },
    });
};

export const getStaffOrders = (cafeId, token) => {
    return axios({
        method: 'POST',
        url: `${HOST}/staff/orders`,
        headers: {
            'Access-Token': TOKEN,
        },
        data: {
            cafeId,
            token,
        },
    });
};

export const changeOrderStatus = (orderId, status) => {
    return axios({
        method: 'PUT',
        url: `${HOST}/order/status/update`,
        headers: {
            'Access-Token': TOKEN,
        },
        data: {
            orderId,
            status,
        },
    });
};

export const getAllCafes = () => {
    return axios({
        method: 'GET',
        url: `${HOST}/cafe`,
        headers: {
            'Access-Token': TOKEN,
        },
    });
};

export const sendQR = qr => {
    return axios({
        method: 'POST',
        url: `${HOST}/qr`,
        headers: {
            'Access-Token': TOKEN,
        },
        data: {
            qr,
        },
    });
};

export const createOrder = (items, total, table, cafeId, date) => {
    return axios({
        method: 'POST',
        url: `${HOST}/order/create`,
        headers: {
            'Access-Token': TOKEN,
        },
        data: {
            items,
            total,
            table,
            cafeId,
            date,
        },
    });
};

export const getOrderStatus = orderId => {
    return axios({
        method: 'GET',
        url: `${HOST}/order/status/${orderId}`,
        headers: {
            'Access-Token': TOKEN,
        },
    });
};

export const getOrdersByArray = orders => {
    return axios({
        method: 'POST',
        url: `${HOST}/orders/array`,
        headers: {
            'Access-Token': TOKEN,
        },
        data: {
            orders,
        },
    });
};

export const getCafeByStaffId = (_id, token) => {
    return axios({
        method: 'POST',
        url: `${HOST}/cafe/staff/id`,
        headers: {
            'Access-Token': TOKEN,
        },
        data: {
            _id,
            token,
        },
    });
};

export const updateMenu = formData => {
    return axios.post(`${HOST}/cafe/menu/edit`, formData, {
        headers: {
            'Access-Token': TOKEN,
            'Content-Type': 'multipart/form-data',
        },
    });
};

export const createMenu = formData => {
    return axios.post(`${HOST}/cafe/menu/create`, formData, {
        headers: {
            'Access-Token': TOKEN,
            'Content-Type': 'multipart/form-data',
        },
    });
};

export const removeFood = (staff_id, token, food_id, imgName) => {
    return axios({
        method: 'POST',
        url: `${HOST}/cafe/menu/remove`,
        headers: {
            'Access-Token': TOKEN,
        },
        data: {
            staff_id,
            token,
            food_id,
            imgName,
        },
    });
};
