export const SET_ORDER = 'SET_ORDER';
export const DELETE_ORDER_ITEM = 'DELETE_ORDER_ITEM';
export const CLEAR_ORDER = 'CLEAR_ORDER';

export const SET_CAFE_INFO = 'SET_CAFE_INFO';
export const CLEAR_CAFE_INFO = 'CLEAR_CAFE_INFO';
