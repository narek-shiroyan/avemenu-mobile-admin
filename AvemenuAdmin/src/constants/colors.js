export const COLORS = {
    coral: '#FF8563',
    darkPurple: '#521990',
    darkPurpleText: '#3A1266',
    grey: '#4D4D4D',
    lightBlue: '#E6F5FF',
    lightOrange: '#E6F5FF',
    lightPurple1: '#F8F2FF',
    lightPurple2: '#EDE1FA',
    lightRed: '#FFE8E8',
    purple: '#9847F2',
    purplePassive: '#A498B3',
    white: '#ffffff',
}