import { createStackNavigator } from '@react-navigation/stack';
import React, { useEffect, useState } from 'react';
import { ActivityIndicator, Platform, View } from 'react-native';
import { getPushToken } from 'src/utils/storage';
import IconButton from '../components/buttons/IconButton';
import { COLORS } from '../constants/colors';
import {
    FOOD_SCREEN,
    HOME_SCREEN,
    LOGIN_SCREEN,
    MENU_SCREEN,
    CREATE_FOOD_SCREEN,
} from '../constants/navigations';
import { checkSign } from '../networks/main';
import HomeScreen from '../screens/Home';
import LoginScreen from '../screens/Login';
import { cleanStaffStorage, getStaffStorage } from '../utils/storage';
import MenuScreen from '../screens/Menu';
import FoodScreen from '../screens/Food';
import CreateFoodScreen from '../screens/CreateFood';

const MainStack = createStackNavigator();

export default function MainNavigator(props) {
    const [loading, setLoading] = useState(true);
    const [sign, setSign] = useState(null);

    useEffect(() => {
        loadStorage();
    }, []);

    if (loading || sign == null) {
        return <ActivityIndicator />;
    }

    const LoginComponentScreen = () => (
        <LoginScreen setSign={setSign} {...props} />
    );

    const HomeComponentScreen = () => (
        <HomeScreen update={props.update} {...props} />
    );

    return (
        <MainStack.Navigator>
            {!sign ? (
                <MainStack.Screen
                    name={LOGIN_SCREEN}
                    options={{ headerShown: false }}
                    component={LoginComponentScreen}
                />
            ) : (
                <>
                    <MainStack.Screen
                        name={HOME_SCREEN}
                        // options={{ headerShown: false }}
                        component={HomeComponentScreen}
                        options={({ navigation }) => ({
                            title: 'Меню',
                            headerStyle: {
                                shadowColor: 'transparent',
                            },
                            headerLeft: () => (
                                <IconButton
                                    name="Logout"
                                    customStyles={{
                                        backgroundColor: COLORS.lightPurple1,
                                        marginLeft: 10,
                                    }}
                                    cb={async () => {
                                        await cleanStaffStorage();
                                        setSign(false);
                                    }}
                                />
                            ),
                            headerRight: () => (
                                <View style={{ flexDirection: 'row' }}>
                                    <IconButton
                                        name="Orders"
                                        customStyles={{
                                            backgroundColor:
                                                COLORS.lightPurple1,
                                            marginRight: 10,
                                        }}
                                        cb={() =>
                                            navigation.navigate(MENU_SCREEN)
                                        }
                                    />
                                    {/*
                                <IconButton
                                    name="CartOutline"
                                    cb={() => { }}
                                    customStyles={{
                                        backgroundColor: COLORS.lightPurple1,
                                        marginRight: 10,
                                    }}
                                /> */}
                                </View>
                            ),
                        })}
                    />

                    {/* <MainStack.Group screenOptions={{ presentation: 'modal' }}> */}
                    <MainStack.Screen
                        name={MENU_SCREEN}
                        //options={{ headerShown: false }}
                        options={({ navigation }) => ({
                            title: 'Меню',
                            // headerStyle: {
                            //     shadowColor: 'transparent',
                            // },
                            headerRight: () => (
                                <View style={{ flexDirection: 'row' }}>
                                    <IconButton
                                        name="Plus"
                                        customStyles={{
                                            backgroundColor:
                                                COLORS.lightPurple1,
                                            marginRight: 10,
                                        }}
                                        cb={() =>
                                            navigation.navigate(
                                                CREATE_FOOD_SCREEN,
                                            )
                                        }
                                    />
                                    {/*
                                <IconButton
                                    name="CartOutline"
                                    cb={() => { }}
                                    customStyles={{
                                        backgroundColor: COLORS.lightPurple1,
                                        marginRight: 10,
                                    }}
                                /> */}
                                </View>
                            ),
                        })}
                        component={MenuScreen}
                    />
                    {/* </MainStack.Group> */}

                    <MainStack.Group screenOptions={{ presentation: 'modal' }}>
                        <MainStack.Screen
                            name={FOOD_SCREEN}
                            //options={{ headerShown: false }}
                            component={FoodScreen}
                        />
                    </MainStack.Group>

                    <MainStack.Group screenOptions={{ presentation: 'modal' }}>
                        <MainStack.Screen
                            name={CREATE_FOOD_SCREEN}
                            //options={{ headerShown: false }}
                            component={CreateFoodScreen}
                        />
                    </MainStack.Group>
                </>
            )}
        </MainStack.Navigator>
    );

    async function loadStorage() {
        try {
            const d = await getStaffStorage();
            const storage = JSON.parse(d);
            console.log('STORAGE', storage);
            if (!storage) {
                setLoading(false);
                return setSign(false);
            }
            if (!storage.token) {
                setLoading(false);
                return setSign(false);
            }
            const push = await getPushToken();
            const os = Platform.OS;

            const { data } = await checkSign(storage.token, push, os);
            if (data && data.status === 'OK' && data.user) {
                setLoading(false);
                setSign(true);
            } else {
                setLoading(false);
                setSign(false);
            }
        } catch (error) {
            setLoading(false);
            setSign(false);
        }
    }
}
