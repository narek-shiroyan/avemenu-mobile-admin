export const getCountOrders = (orders) => {
    let count = 0;
    for (const key in orders) {
        if (Object.hasOwnProperty.call(orders, key)) {
            const element = orders[key];
            count += element.counter;
        }
    }
    return count;
}