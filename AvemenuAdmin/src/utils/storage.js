import AsyncStorage from '@react-native-async-storage/async-storage';

export const setStaff = async value => {
    try {
        const jsonValue = JSON.stringify(value);
        await AsyncStorage.setItem('@staff', jsonValue);
    } catch (e) {
        // saving error
        console.log(e);
    }
};

export const getStaffStorage = async () => {
    try {
        const value = await AsyncStorage.getItem('@staff');
        if (value !== null) {
            // value previously stored
            return value;
        }
    } catch (e) {
        // error reading value
        console.log(e);
    }
};

export const cleanStaffStorage = async () => {
    try {
        await AsyncStorage.removeItem('@staff');
    } catch (e) {
        // remove error
        console.log(e);
    }
};

// Получить пуш токен
export const getPushToken = async () => {
    try {
        const token = await AsyncStorage.getItem('@push_token');
        if (token !== null) {
            return token;
        }
        return null;
    } catch (e) {
        console.log(e);
    }
};

// Запись пуш токена
export const setPushToken = async token => {
    // console.log('setPushToken', token);
    try {
        await AsyncStorage.setItem('@push_token', token);
    } catch (e) {
        console.log(e);
    }
};
