export const markers = [
    {
        id: 1,
        lat: 55.383152,
        lon: 36.72807,
        address: 'ул. Ленина, 8, микрорайон Южный, Наро-Фоминск',
        title: 'Паприка',
        description:
            'Заказывайте на доставку у нас ваши любимые блюда. Почувствуйте частичку солнечной Италии на вашем столе, ведь мы используем только свежие и качественные продукты. Мы готовим пасту и пиццу в классическом итальянском стиле. И делаем это с любовью! Buon Appetito!!!',
        work_time: '10:00 - 23:30',
        phone: '+7(964)-553-51-51',
        images: [
            'https://welcome.mosreg.ru/cache/thumbs/thumb_666_475_4115414276.jpg',
        ],
        website: 'https://paprika-nf.ru/',
    },
    {
        id: 2,
        lat: 55.411138,
        lon: 36.713705,
        address: 'Кубинское ш., 5, Наро-Фоминск',
        title: 'Домино',
        description:
            'Кафе Домино - доставка шашлыка, блюд кавказской и европейской кухни по Наро-Фоминску и району',
        work_time: '10:00 - 23:00',
        phone: '79660990933',
        images: [
            'https://avatars.mds.yandex.net/get-altay/1678797/2a0000016924cacf364253bdff59841a3acc/XXL',
            'https://avatars.mds.yandex.net/get-altay/1439437/2a0000016924cbd13ced423ea0f76d23309f/XXL',
        ],
        website: 'https://order.cafe-domino.ru/',
        menu: [
            {
                _id: 1,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной шейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 435,
                group: 'Блюда на огне',
            },
            {
                _id: 2,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной корейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Блюда на огне',
            },
            {
                _id: 3,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свинины ребра',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Блюда на огне',
            },
            {
                _id: 4,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной шейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 435,
                group: 'Первые блюда',
            },
            {
                _id: 5,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной корейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Первые блюда',
            },
            {
                _id: 6,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свинины ребра',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Первые блюда',
            },
            {
                _id: 7,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной шейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 435,
                group: 'Вторые блюда',
            },
            {
                _id: 8,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной корейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Вторые блюда',
            },
            {
                _id: 9,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свинины ребра',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Вторые блюда',
            },
            {
                _id: 10,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной шейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 435,
                group: 'Салаты',
            },
            {
                _id: 11,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной корейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Салаты',
            },
            {
                _id: 12,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свинины ребра',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Салаты',
            },
            {
                _id: 13,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной шейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 435,
                group: 'Горячие салаты',
            },
            {
                _id: 14,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной корейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Горячие салаты',
            },
            {
                _id: 15,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свинины ребра',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Горячие салаты',
            },
            {
                _id: 16,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной шейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 435,
                group: 'Гарниры',
            },
            {
                _id: 17,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной корейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Гарниры',
            },
            {
                _id: 18,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свинины ребра',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Гарниры',
            },
            {
                _id: 19,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной шейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 435,
                group: 'Холодные закуски',
            },
            {
                _id: 20,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной корейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Холодные закуски',
            },
            {
                _id: 21,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свинины ребра',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Холодные закуски',
            },
            {
                _id: 22,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной шейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 435,
                group: 'Соусы',
            },
            {
                _id: 23,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной корейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Соусы',
            },
            {
                _id: 24,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свинины ребра',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Соусы',
            },
            {
                _id: 25,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной шейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 435,
                group: 'Выпечка',
            },
            {
                _id: 25,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной корейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Выпечка',
            },
            {
                _id: 26,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свинины ребра',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Выпечка',
            },
            {
                _id: 27,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной шейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 435,
                group: 'Напитки',
            },
            {
                _id: 28,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной корейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Напитки',
            },
            {
                _id: 29,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свинины ребра',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Напитки',
            },
            {
                _id: 30,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной шейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 435,
                group: 'Избранное',
            },
            {
                _id: 31,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свиной корейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Избранное',
            },
            {
                _id: 32,
                variants: [],
                images: [
                    'https://www.delivery-club.ru/media/cms/relation_product/37402/324746407_m650.jpg',
                ],
                name: 'Шашлык из свинины ребра',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Избранное',
            },
        ],
    },
    {
        id: 3,
        lat: 55.379173,
        lon: 36.735005,
        title: 'Прайд',
        address: 'ул. Профсоюзная Д.1, Наро-Фоминск',
        description:
            'Кафе Домино - доставка шашлыка, блюд кавказской и европейской кухни по Наро-Фоминску и району',
        work_time: '10:00 - 23:00',
        phone: '+7 (496) 347-38-38',
        images: [
            'https://sun9-33.userapi.com/impf/c637623/v637623038/169d8/UM0ClL6xPwY.jpg?size=604x338&quality=96&sign=1610b03a4ddbeb2a59d44830df0ae7e7&type=album',
        ],
        website: 'http://pride-pub.ru/',
    },
];
