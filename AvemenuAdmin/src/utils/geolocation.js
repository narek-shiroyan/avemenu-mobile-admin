import GetLocation from 'react-native-get-location';

/**
 * Функция выдачи текущик координат
 * @return {lat, lon}
 */
export const getCurrentPosition = async () => {
    try {
        const position = await GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        });
        return position;
    } catch (e) {
        console.warn(e);
    }
};
