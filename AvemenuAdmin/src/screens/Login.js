import { Input, Stack } from 'native-base';
import React, { useState } from 'react';
import {
    Alert,
    Image,
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    KeyboardAvoidingView,
} from 'react-native';
import CoralButton from '../components/buttons/CoralButton';
import { FONTS } from '../constants/fonts';
import { DEVICE_WIDTH } from '../constants/size';
import Container from '../layouts/Container';
import { signIn } from '../networks/main';
import { setStaff } from '../utils/storage';
import { getPushToken } from 'src/utils/storage';

export default function LoginScreen(props) {
    const [login, setLogin] = useState(null);
    const [password, setPassword] = useState(null);
    //const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : 50;
    const keyboardVerticalOffset = Platform.OS === 'ios' ? 60 : 0;

    return (
        <Container>
            <KeyboardAvoidingView
                behavior={Platform.OS === 'android' ? 'height' : 'position'}
                keyboardVerticalOffset={keyboardVerticalOffset}
                style={styles.container2}
            >
                <ScrollView>
                    <View style={styles.container}>
                        <Image
                            source={require('../assets/login.png')}
                            style={styles.image}
                        />
                        <Text style={styles.title__text}>Войти в систему</Text>
                        <Stack space={4} w="100%" alignItems="center">
                            <Input
                                w={{
                                    base: '100%',
                                    md: '55%',
                                }}
                                onChangeText={text => setLogin(text)}
                                variant="underlined"
                                placeholder="Логин"
                                style={{ marginTop: 20, fontSize: 16 }}
                            />
                            <Input
                                w={{
                                    base: '100%',
                                    md: '55%',
                                }}
                                onChangeText={text => setPassword(text)}
                                type={'password'}
                                variant="underlined"
                                placeholder="Пароль"
                                style={{
                                    marginTop: 20,
                                    fontSize: 16,
                                    marginBottom: 10,
                                }}
                            />
                            <CoralButton name={'ВОЙТИ'} cb={sign} />
                        </Stack>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        </Container>
    );

    async function sign() {
        const push = await getPushToken();
        const os = Platform.OS;

        const { data } = await signIn(
            login.toLowerCase(),
            password,
            push ? push : null,
            os,
        );

        if (!data) {
            return Alert.alert('Ошибка сервера');
        }
        if (data.status === 'ERROR') {
            return Alert.alert(data.message);
        }

        await setStaff(data.user);
        await props.setSign(true);
    }
}

const styles = StyleSheet.create({
    image: {
        width: DEVICE_WIDTH / 2,
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    container2: {
        width: '100%',
    },
    container: {
        alignItems: 'center',
        paddingHorizontal: 15,
    },
    title__text: {
        ...FONTS.btnCaption.h2_24px_dark,
    },
});
