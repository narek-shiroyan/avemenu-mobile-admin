import { Button } from 'native-base';
import React, { useState } from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import EditIcon from '../assets/icons/20x20/edit.svg';
import IconCustom38 from '../components/icons/38x38';
import { COLORS } from '../constants/colors';
import { FONTS } from '../constants/fonts';
import { DEVICE_HEIGHT } from '../constants/size';
import Container from '../layouts/Container';
import { createMenu } from '../networks/main';
import { getStaffStorage } from '../utils/storage';

const keyboardVerticalOffset = Platform.OS === 'ios' ? 60 : 100;

export default function CreateFoodScreen(props) {
    const [store, setStore] = useState({
        images: [],
        name: null,
        portion: null,
        description: null,
        group: null,
        price: 0,
    });
    const [imageUri, setImageUri] = useState(null);
    // console.log(imageUri);
    // console.log('STORE', store);
    return (
        <Container>
            {/* <IconButton
                name="ArrowLeft"
                size={20}
                customStyles={styles.backBottom}
                cb={() => props.navigation.goBack()}
            /> */}
            <KeyboardAvoidingView
                behavior={Platform.OS === 'android' ? 'height' : 'position'}
                keyboardVerticalOffset={keyboardVerticalOffset}
            >
                <ScrollView>
                    <TouchableOpacity onPress={pickImage}>
                        <Image
                            source={{
                                uri: !imageUri
                                    ? null
                                    : `${
                                          Platform.OS === 'android'
                                              ? imageUri.path
                                              : imageUri.sourceURL
                                      }`,
                            }}
                            style={styles.image}
                            resizeMode="cover"
                            resizeMethod="scale"
                        />
                        {imageUri ? (
                            <View style={styles.clear__btn__container}>
                                <TouchableOpacity
                                    onPress={() => setImageUri(null)}
                                >
                                    <Text style={styles.clear__btn__text}>
                                        Очистить
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        ) : null}
                    </TouchableOpacity>

                    <View style={styles.block}>
                        <TextInput
                            value={store.name}
                            placeholder={'Введите название'}
                            onChangeText={text =>
                                setStore({
                                    ...store,
                                    name: text,
                                })
                            }
                            style={styles.title}
                        />
                        <View style={styles.portion__block}>
                            <IconCustom38
                                name="Dish"
                                size={38}
                                style={{ alignSelf: 'center' }}
                            />
                            <TextInput
                                value={store.portion}
                                placeholder={'Введите порцию'}
                                onChangeText={text =>
                                    setStore({
                                        ...store,
                                        portion: text,
                                    })
                                }
                                style={styles.text__portion}
                            />
                        </View>

                        {/* Описание */}
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.about__title}>
                                Описание <EditIcon />{' '}
                            </Text>
                            <TextInput
                                value={store.description}
                                placeholder={'Введите описание'}
                                onChangeText={text =>
                                    setStore({
                                        ...store,
                                        description: text,
                                    })
                                }
                                multiline
                                style={styles.about__description}
                            />
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.about__title}>
                                Стоимость <EditIcon />
                            </Text>
                            <TextInput
                                value={`${store.price}`}
                                placeholder={'Введите стоимость'}
                                keyboardType={'numeric'}
                                onChangeText={text =>
                                    setStore({
                                        ...store,
                                        price: text,
                                    })
                                }
                                style={styles.about__description}
                            />
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.about__title}>
                                Категория <EditIcon />
                            </Text>
                            <TextInput
                                value={store.group}
                                placeholder={'Введите категорию'}
                                onChangeText={text =>
                                    setStore({
                                        ...store,
                                        group: text,
                                    })
                                }
                                style={styles.about__description}
                            />
                        </View>
                        <View style={{ marginTop: 20, marginBottom: 50 }}>
                            <Button onPress={save}>Сохранить</Button>
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
            {/* <BottomPrice food={food} cb={addToCard} /> */}
        </Container>
    );

    function pickImage() {
        ImagePicker.openPicker({
            width: 300,
            height: 320,
            cropping: true,
            multiple: false,
            includeBase64: true,
        }).then(image => {
            // console.log(image);
            setImageUri(image);
        });
    }
    async function save() {
        const d = await getStaffStorage();
        const staff = JSON.parse(d);

        let formData = new FormData();
        formData.append('staff_id', staff._id);
        formData.append('token', staff.token);
        formData.append('items', JSON.stringify(store));
        // console.log(imageUri);
        if (imageUri) {
            formData.append('file', JSON.stringify(imageUri));
        }

        const { data } = await createMenu(formData);

        if (!data) {
            return Alert.alert('Ошибка сервера');
        }

        if (data.status !== 'OK') {
            return Alert.alert('Произошла ошибка', data.message);
        }

        return Alert.alert('Успешно сохранено');
    }
}

const styles = StyleSheet.create({
    image: {
        height: DEVICE_HEIGHT / 2.5,
        width: '100%',
    },
    buttons__container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 15,
    },
    removeButton: {
        backgroundColor: 'red',
        color: COLORS.white,
        flexDirection: 'row',
        justifyContent: 'center',
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 5,
    },
    stopButton: {
        backgroundColor: COLORS.lightRed,
        flexDirection: 'row',
        justifyContent: 'center',
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 5,
    },
    clear__btn__container: {
        position: 'absolute',
        bottom: 30,
        alignSelf: 'center',
    },
    clear__btn__text: {
        color: '#fff',
        ...FONTS.p_18px_white_center,
    },
    block: {
        width: '100%',
        height: '100%',
        borderTopRightRadius: 24,
        borderTopLeftRadius: 24,
        marginTop: -20,
        backgroundColor: COLORS.white,
        paddingTop: 30,
        paddingHorizontal: 15,
    },
    backBottom: {
        position: 'absolute',
        top: 10,
        left: 10,
        zIndex: 20,
    },
    portion__block: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'flex-start',
        height: 50,
        marginTop: 20,
    },
    title: {
        ...FONTS.btnCaption.h3_20px_dark,
    },
    text__portion: {
        ...FONTS.secondary_13px_text,
        alignSelf: 'center',
        marginLeft: 5,
    },

    about__title: {
        ...FONTS.btnCaption.h4_16px_dark,
    },
    about__description: {
        ...FONTS.p_14px_text,
        marginTop: 10,
    },
});
