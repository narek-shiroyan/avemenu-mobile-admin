import _ from 'lodash';
import { Center, Heading } from 'native-base';
import React, { useEffect, useState } from 'react';
import {
    ActivityIndicator,
    Alert,
    Image,
    SectionList,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { useToast } from 'native-base';
import CancelIcon from '../assets/icons/16x16/cancelled.svg';
import WaitIcon from '../assets/icons/16x16/clock.svg';
import SuccessIcon from '../assets/icons/16x16/done.svg';
import EndIcon from '../assets/icons/16x16/end.svg';
import PressButton from '../components/buttons/PressButton';
import { COLORS } from '../constants/colors';
import { CURRENCY } from '../constants/currency';
import { FONTS } from '../constants/fonts';
import { DEVICE_HEIGHT } from '../constants/size';
import Container from '../layouts/Container';
import { changeOrderStatus, getStaffOrders } from '../networks/main';
import { getImage } from '../utils/images';
import { getStaffStorage } from '../utils/storage';

export default function HomeScreen(props) {
    const toast = useToast();
    const [loading, setLoading] = useState(true);
    const [cafe, setCafe] = useState(null);
    const [orders, setOrders] = useState(null);
    let con = props.update;
    useEffect(() => {
        loadData();
        if (!con !== props.update) {
            toast.show({
                title: 'Новый заказ',
                status: 'success',
                // description: "Thanks for signing up with us.",
            });
        }
    }, [props.update]);


    if (loading) {
        return <ActivityIndicator />;
    }
    return (
        <Container>
            <SectionList
                px="12"
                mb="4"
                sections={orders}
                contentContainerStyle={{ paddingHorizontal: 10 }}
                keyExtractor={(item, index) => item + index}
                onRefresh={loadData}
                refreshing={loading}
                renderItem={({ item }) => {
                    let date = new Date(Number(item.date));
                    let time = {
                        day: date.getDate(),
                        month: date.getMonth() + 1,
                        year: date.getFullYear(),
                        hour: date.getHours(),
                        minutes: date.getMinutes(),
                    };

                    return (
                        <View style={styles.order__container}>
                            <Text style={styles.order__text}>Заказ</Text>

                            <View style={styles.pay__text}>
                                {renderOrderStatus(item.status)}
                                <Text
                                    style={{
                                        ...FONTS.secondary_13px_text_passive,
                                        marginLeft: 5,
                                    }}
                                >
                                    / {time.day}.{time.month}.{time.year},{' '}
                                    {time.hour}:{time.minutes}
                                </Text>
                            </View>

                            {item.items.map((el, key) => (
                                <View key={key} style={styles.item__container}>
                                    <View style={styles.image__container}>
                                        {el.images ? (
                                            <Image
                                                source={{
                                                    uri: getImage(
                                                        el.images[0],
                                                        250,
                                                        200,
                                                    ),
                                                }}
                                                resizeMethod="auto"
                                                resizeMode="cover"
                                                style={styles.image}
                                            />
                                        ) : (
                                            <View style={styles.defaultImage} />
                                        )}
                                    </View>
                                    <View style={styles.text__container}>
                                        <Text style={styles.title__text}>
                                            {el.name}
                                        </Text>
                                        <Text style={styles.portion__text}>
                                            {el.portion}
                                        </Text>
                                        <View style={{ marginTop: 10 }}>
                                            <Text
                                                style={{ ...FONTS.p_14px_text }}
                                            >
                                                {el.counter} x{' '}
                                                <Text
                                                    style={{
                                                        ...FONTS.secondary_14px_price_cart,
                                                    }}
                                                >
                                                    {' '}
                                                    {el.price} {CURRENCY.ru}
                                                </Text>
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            ))}
                            <View style={styles.button__container}>
                                {renderButton(item.status, item._id)}
                            </View>
                        </View>
                    );
                }}
                renderSectionHeader={({ section: { title } }) => (
                    <Center style={{ backgroundColor: COLORS.white }}>
                        <Heading fontSize="xl" mt="8" pb="4">
                            Стол №{title} 
                        </Heading>
                    </Center>
                )}
            />
        </Container>
    );

    async function loadData() {
        const storage = await getStaffStorage();

        let json = JSON.parse(storage);
        // Загрузить инфу из API
        const { data } = await getStaffOrders(json.cafe, json.token);

        if (!data) {
            return Alert.alert('Ошибка сервера');
        }

        if (data.status === 'ERROR') {
            return Alert.alert(data.message);
        }

        if (data.status === 'OK') {
            let newItem = [];
            const d = _.groupBy(data.items.orders, function (b) {
                return b.table;
            });
            // delete d['Хлеб'];
            // delete d['Выпечка'];
            // delete d['Напитки'];
            // delete d['Гарниры'];
            // delete d['Холодные закуски}'];
            for (const key in d) {
                if (Object.hasOwnProperty.call(d, key)) {
                    const element = d[key];
                    newItem.push({
                        title: key,
                        data: element,
                    });
                }
            }

            setCafe(data.items.cafe);
            setOrders(newItem);
            setLoading(false);
        }
    }

    function renderButton(status, orderId) {
        if (status === 'wait') {
            return (
                <>
                    <PressButton
                        title={'Принять'}
                        type={'ok'}
                        cb={async () => {
                            const { data } = await changeOrderStatus(
                                orderId,
                                'accept',
                            );
                            console.log('DATA', data);
                            await loadData();
                        }}
                        customStyles={{ width: '40%' }}
                    />
                    <PressButton
                        title={'Отменить'}
                        type={'cancel'}
                        cb={async () => {
                            await changeOrderStatus(orderId, 'cancel');
                            await loadData();
                        }}
                        customStyles={{ width: '40%' }}
                    />
                </>
            );
        }
        if (status === 'accept') {
            return (
                <>
                    <PressButton
                        title={'Заказ оплачен'}
                        type={'ok'}
                        cb={async () => {
                            await changeOrderStatus(orderId, 'end');
                            await loadData();
                        }}
                        customStyles={{ width: '40%' }}
                    />
                    <PressButton
                        title={'Отменить'}
                        type={'cancel'}
                        cb={async () => {
                            await changeOrderStatus(orderId, 'cancel');
                            await loadData();
                        }}
                        customStyles={{ width: '40%' }}
                    />
                </>
            );
        }
        return null;
    }

    function renderOrderStatus(s) {
        if (s === 'wait') {
            return (
                <>
                    <WaitIcon size={16} />
                    <Text style={styles.order__status__desctiption}>
                        Ожидает подтверждения
                    </Text>
                </>
            );
        }
        if (s === 'end') {
            return (
                <>
                    <EndIcon size={16} />
                    <Text style={styles.order__status__desctiption}>
                        Заказ завершен
                    </Text>
                </>
            );
        }
        if (s === 'accept') {
            return (
                <>
                    <SuccessIcon size={16} />
                    <Text style={styles.order__status__desctiption}>
                        Заказ принят
                    </Text>
                </>
            );
        }

        if (s === 'cancel') {
            return (
                <>
                    <CancelIcon size={16} />
                    <Text style={styles.order__status__desctiption}>
                        Заказ отменен
                    </Text>
                </>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
    },
    order__container: {
        borderColor: COLORS.lightPurple2,
        borderWidth: 3,
        borderRadius: 8,
        paddingHorizontal: 15,
        marginVertical: 20,
    },
    button__container: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 20,
    },
    pay__text: {
        ...FONTS.secondary_13px_text,
        marginTop: 10,
        marginBottom: 20,
        flexDirection: 'row',
    },
    order__text: {
        ...FONTS.links.link_16px_normal,
        marginTop: 20,
    },
    loading__container: {
        justifyContent: 'center',
        height: DEVICE_HEIGHT,
    },
    title__container: {
        marginTop: 25,
    },
    scrollview: {
        marginTop: 20,
        height: DEVICE_HEIGHT / 1.2,
    },
    title: {
        ...FONTS.btnCaption.h2_24px_dark,
    },
    emptyContainer: {
        paddingHorizontal: 20,
        marginTop: 100,
        //justifyContent: 'center',
        width: '100%',
        height: '100%',
        alignItems: 'center',
    },

    empty__main_text: {
        ...FONTS.btnCaption.h2_24px_dark,
        marginTop: 20,
    },
    empty__subtext: {
        ...FONTS.p_16px_dark_center,
        marginTop: 10,
    },
    empty__image: {
        height: DEVICE_HEIGHT / 3,
        resizeMode: 'contain',
        //backgroundColor: '#f2f',
    },
    coralBtn: {
        marginTop: 20,
        width: '100%',
    },
    item__container: {
        width: '100%',
        //height: 250,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginVertical: 5,
    },
    image__container: {
        width: 123,
        height: 140,
        borderRadius: 16,
    },
    image: {
        width: '100%',
        height: '100%',
        borderRadius: 16,
    },
    order__status__desctiption: {
        marginLeft: 5,
    },
    price__container: {
        width: 50,
        height: 30,
        backgroundColor: COLORS.coral,
        position: 'absolute',
        bottom: 0,
        borderBottomLeftRadius: 16,
        borderTopRightRadius: 16,
        justifyContent: 'center',
    },
    price__text: {
        ...FONTS.secondary_14px_price_old,
        alignSelf: 'center',
    },
    title__text: {
        ...FONTS.btnCaption.h4_16px_dark,
    },
    portion__text: {
        ...FONTS.secondary_13px_text_passive,
        marginTop: 10,
    },
    text__container: {
        height: 60,
        width: '65%',
        marginTop: 30,
        marginLeft: 20,
    },
    defaultImage: {
        width: 123,
        height: 140,
        borderRadius: 16,
        backgroundColor: COLORS.lightPurple2,
    },
});
