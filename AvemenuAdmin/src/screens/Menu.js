import React, { useEffect, useState } from 'react';
import { useIsFocused } from '@react-navigation/native';
import {
    ActivityIndicator,
    Alert,
    FlatList,
    StyleSheet,
    View,
} from 'react-native';
import FoodCart from '../components/cart/FoodCart';
import { FOOD_SCREEN } from '../constants/navigations';
import { DEVICE_WIDTH } from '../constants/size';
import Container from '../layouts/Container';
import { getCafeByStaffId } from '../networks/main';
import { getStaffStorage } from '../utils/storage';

export default function MenuScreen(props) {
    const [loading, setLoading] = useState(true);
    const [menu, setMenu] = useState([]);
    const isFocused = useIsFocused();

    useEffect(() => {
        if (isFocused) {
            loadData();
        }
    }, [isFocused]);

    if (loading) {
        return (
            <Container>
                <ActivityIndicator />
            </Container>
        );
    }
    return (
        <Container>
            <FlatList
                data={menu}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => {
                    function click() {
                        props.navigation.navigate(FOOD_SCREEN, {
                            food: item,
                        });
                    }
                    return (
                        <View style={{ margin: 10 }}>
                            <FoodCart {...item} click={click} />
                        </View>
                    )
                }}
                numColumns={2}
                style={{
                    width: DEVICE_WIDTH,
                }}
                contentContainerStyle={{
                    alignItems: 'center',
                }}
            />
        </Container>
    );

    async function loadData() {
        const d = await getStaffStorage();
        const staff = JSON.parse(d);
        //console.log(staff._id, staff.token);
        // Получить список с меню
        const { data } = await getCafeByStaffId(staff._id, staff.token);
        if (!data) {
            setLoading(false);
            return Alert.alert('Ошибка сервера');
        }

        if (data.status !== 'OK') {
            setLoading(false);
            return Alert.alert(data.message);
        }
        setLoading(false);
        return setMenu(data.c.menu);
        // console.log('test', data.c.menu);
    }
}

const styles = StyleSheet.create({});
