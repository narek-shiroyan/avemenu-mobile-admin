import React from 'react';
import {
    Stack,
    Alert,
    HStack,
    VStack,
    Text,
    Divider,
    ScrollView,
    Center,
    NativeBaseProvider,
} from 'native-base';

export default function NewOrder() {
    return (
        <Alert
            w="100%"
            variant={'top-accent'}
            colorScheme="success"
            status="success"
            style={{position: 'absolute', top: 50, left: 0, zIndex: 20 }}
        >
            <VStack space={2} flexShrink={1} w="100%">
                <HStack
                    flexShrink={1}
                    space={2}
                    alignItems="center"
                    justifyContent="space-between"
                >
                    <HStack space={2} flexShrink={1} alignItems="center">
                        <Alert.Icon />
                        <Text color={'coolGray.800'}>Новый заказ</Text>
                    </HStack>
                </HStack>
            </VStack>
        </Alert>
    );
}
