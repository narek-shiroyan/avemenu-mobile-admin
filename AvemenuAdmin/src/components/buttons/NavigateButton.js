import { Icon } from 'native-base';
import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import FA from 'react-native-vector-icons/FontAwesome5';
import { COLORS } from '../../constants/colors';

export default function NavigateButton({ cb }) {
    return (
        <TouchableOpacity onPress={cb} style={styles.container}>
            <Icon as={FA} name="location-arrow" size={3} color={COLORS.coral} />
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 250,
        right: 30,
        width: 30,
        height: 30,
        backgroundColor: COLORS.white,
        justifyContent: 'center',
        borderRadius: 8,
        alignItems: 'center',
    },
});
