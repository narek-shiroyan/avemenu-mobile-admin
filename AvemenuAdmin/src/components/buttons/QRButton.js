import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import QRIcon from '../../assets/icons/qr2.svg';
import { COLORS } from '../../constants/colors';

export default function QRButton({ cb }) {
    return (
        <TouchableOpacity onPress={cb} style={styles.container}>
            <QRIcon style={styles.image} />
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        width: 80,
        height: 80,
        backgroundColor: COLORS.coral,
        borderRadius: 50,
        justifyContent: 'center',
        position: 'absolute',
        bottom: 100,
        alignSelf: 'center',
    },
    image: {
        width: 40,
        height: 40,
        alignSelf: 'center',
    },
});
