import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { COLORS } from '../../constants/colors';
import IconCustom from '../icons/20x20';
import { Circle } from 'native-base';

export default function IconButton({
    name,
    customStyles = null,
    cb,
    size,
    count,
}) {
    return (
        <TouchableOpacity style={[styles.container, customStyles]} onPress={cb}>
            {/* <Image source={icons[name]} style={styles.icon} /> */}
            <IconCustom
                name={name}
                size={!size ? 20 : size}
                style={styles.icon}
            />
            {count ? counter() : null}
        </TouchableOpacity>
    );

    function counter() {
        return (
            <Circle style={styles.badge} size={5} bg={COLORS.coral}>
                <Text style={styles.text}>{count}</Text>
            </Circle>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: 44,
        height: 44,
        backgroundColor: COLORS.white,
        justifyContent: 'center',
        borderRadius: 8,
    },
    icon: {
        width: 20,
        alignSelf: 'center',
    },
    badge: {
        position: 'absolute',
        bottom: -5,
        right: -5,
    },
    text: {
        color: COLORS.white,
    },
});
