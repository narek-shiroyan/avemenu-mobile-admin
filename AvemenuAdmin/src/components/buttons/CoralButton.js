import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { COLORS } from '../../constants/colors';
import { FONTS } from '../../constants/fonts';

export default function CoralButton(props) {
    const { name, cb } = props;
    return (
        <TouchableOpacity onPress={cb} style={styles.container}>
            <Text style={styles.text}>{name}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: COLORS.coral,
        alignSelf: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        height: 60,
        borderRadius: 8,
    },
    text: {
        alignSelf: 'center',
        ...FONTS.btnCaption.white15px,
    },
});
