import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { COLORS } from '../../constants/colors';
import { FONTS } from '../../constants/fonts';

export default function PressButton({ title, type, cb, customStyles }) {
    return (
        <TouchableOpacity onPress={cb} style={[styles.container, styles[type], customStyles ]}>
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        alignSelf: 'center',
        justifyContent: 'center',
        backgroundColor: COLORS.coral,
        height: 30,
        borderRadius: 5,
        margin: 5,
    },
    text: {
        alignSelf: 'center',
        ...FONTS.btnCaption.white15px,
    },
    cancel: {
        backgroundColor: 'red',
    },
    ok: {
        backgroundColor: COLORS.coral,
    },
});
