import React from 'react';
import { TouchableOpacity } from 'react-native';
import IconCustom38 from '../icons/38x38';

export default function CircleBtn(props) {
    const { iconName, cb, customStyles } = props;
    return (
        <TouchableOpacity onPress={cb} style={customStyles}>
            <IconCustom38 name={iconName} size={38} />
        </TouchableOpacity>
    );
}
