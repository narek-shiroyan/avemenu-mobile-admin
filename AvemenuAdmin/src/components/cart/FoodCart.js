import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { COLORS } from '../../constants/colors';
import { FONTS } from '../../constants/fonts';
import { CURRENCY } from '../../constants/currency';
import { getImage } from '../../utils/images';

export default function FoodCart(props) {
    const { images, name, price, portion, click, stop } = props;
    return (
        <TouchableOpacity onPress={click} style={styles.container}>
            <View style={styles.image__container}>
                {images.length ? (
                    <Image
                        source={{
                            uri: images.length
                                ? getImage(images[0], 250, 200)
                                : null,
                        }}
                        resizeMethod="auto"
                        resizeMode="cover"
                        style={styles.image}
                    />
                ) : (
                    <View style={styles.defaultImage} />
                )}

                {
                    stop ? (<View style={styles.stopContainer}>
                        <Text style={styles.stopText}>СТОП</Text>
                    </View>) : null
                }
                <View style={styles.price__container}>
                    <Text style={styles.price__text}>
                        {price}
                        {CURRENCY.ru}
                    </Text>
                </View>
            </View>
            <View style={styles.text__container}>
                <Text style={styles.title__text}>{name}</Text>
                <Text style={styles.portion__text}>{portion}</Text>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        width: 165,
        height: 250,
        //backgroundColor: '#f2f',
    },
    image__container: {
        width: 163,
        height: 180,
        borderRadius: 16,
    },
    image: {
        width: '100%',
        height: '100%',
        borderRadius: 16,
    },
    defaultImage: {
        width: 163,
        height: 180,
        borderRadius: 16,
        backgroundColor: COLORS.lightPurple2,
    },
    price__container: {
        width: 50,
        height: 30,
        backgroundColor: COLORS.coral,
        position: 'absolute',
        bottom: 0,
        borderBottomLeftRadius: 16,
        borderTopRightRadius: 16,
        justifyContent: 'center',
    },
    stopContainer: {
        position: 'absolute',
        backgroundColor: 'rgba(211,211,211,0.5)',
        zIndex: 50,
        width: '100%',
        height: '100%',
        borderRadius: 16,
        justifyContent: 'center',
    },
    stopText: {
        ...FONTS.btnCaption.h3_20px_white,
        alignSelf: 'center',
    },
    price__text: {
        ...FONTS.secondary_14px_price_old,
        alignSelf: 'center',
    },
    title__text: {
        ...FONTS.btnCaption.h4_16px_dark,
    },
    portion__text: {
        ...FONTS.secondary_13px_text_passive,
    },
    text__container: {
        height: 60,
        width: '98%',
        marginTop: 10,
    },
});
