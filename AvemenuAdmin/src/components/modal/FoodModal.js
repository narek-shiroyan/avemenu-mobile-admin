import React, { forwardRef, useRef, useState } from 'react';
import { Image, Linking, StyleSheet, Text, View } from 'react-native';
import { Modalize } from 'react-native-modalize';
import { COLORS } from '../../constants/colors';
import { FONTS } from '../../constants/fonts';
import { useCombinedRefs } from '../../utils/use-combined-refs';
import IconButton from '../buttons/IconButton';
import IconCustomCoral from '../icons/20x20_coral';

/**
 * name: 'Шашлык из свиной корейки',
                description:
                    'Сочные куски свиной шеи, обжаренные на огне до насыщенного мясного вкуса, подаётся с луком и зеленью.',
                portion: '300 г',
                price: 420,
                group: 'Блюда на огне',
 */
export const FoodModal = forwardRef((props, ref) => {
    const { name, description, } = props;
    const modalizeRef = useRef(null);
    const combinedRef = useCombinedRefs(ref, modalizeRef);
    const [toggle, setToggle] = useState(true);

    const handleClose = () => {
        if (combinedRef.current) {
            combinedRef.current.close();
        }
    };


    const renderContent = () => (
        <View style={styles.content}>
            <Image
                source={{
                    uri: `${images}`,
                }}
                style={styles.cafeImage}
            />
            <View style={styles.titleContainer}>
                <View style={styles.title__block}>
                    <Text style={styles.title}>{title}</Text>
                </View>
                <View style={styles.buttonsGroup}>
                    <View style={styles.btn__item}>
                        <IconButton
                            name="Globe"
                            customStyles={{
                                backgroundColor: COLORS.lightPurple1,
                            }}
                            cb={() => Linking.openURL(website)}
                        />
                    </View>

                    <View style={styles.btn__item}>
                        <IconButton
                            name="PhoneCall"
                            customStyles={{
                                backgroundColor: COLORS.lightPurple1,
                            }}
                            cb={() => Linking.openURL(`tel:${phone}`)}
                        />
                    </View>
                </View>
            </View>

            {renderAddress()}
            {renderWorkTime()}
            {/* <Text style={styles.content__subheading}>{title}</Text>
            <Text style={styles.content__heading}>{address}</Text>
            <Text style={styles.content__description}>{work_time}</Text> */}

            {/* <TouchableOpacity
                style={s.content__description}
                activeOpacity={0.75}
                onPress={() => setToggle(!toggle)}
            >
                <Text>adjustToContentHeight {JSON.stringify(toggle)}</Text>
            </TouchableOpacity> */}
        </View>
    );

    return (
        <Modalize ref={combinedRef} adjustToContentHeight={toggle}>
            {renderContent()}
        </Modalize>
    );

    function renderAddress() {
        if (!address) {
            return null;
        }
        return (
            <View style={styles.address__container}>
                <View style={styles.address__icon_container}>
                    <IconCustomCoral name="Pin" size={20} />
                </View>
                <View style={styles.address__text__block}>
                    <Text style={styles.address__text}>{address}</Text>
                </View>
            </View>
        );
    }

    function renderWorkTime() {
        if (!work_time) {
            return null;
        }
        return (
            <View style={styles.time__container}>
                <View style={styles.address__icon_container}>
                    <IconCustomCoral name="Clock" size={20} />
                </View>
                <View style={styles.address__text__block}>
                    <Text style={styles.address__text}>{work_time}</Text>
                </View>
            </View>
        );
    }
});

const styles = StyleSheet.create({
    content: {
        padding: 20,
    },
    title: {
        ...FONTS.btnCaption.h3_20px_dark,
    },
    title__block: {
        justifyContent: 'center',
        width: '60%',
    },
    titleContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        marginTop: 10,
    },
    buttonsGroup: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    btn__item: {
        justifyContent: 'center',
        marginHorizontal: 8,
        marginRight: 0,
    },
    address__text__block: {
        justifyContent: 'center',
        width: '85%',
    },
    address__container: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: '100%',
        marginTop: 20,
    },
    address__icon_container: {
        justifyContent: 'center',
        marginRight: 5,
    },
    address__text: {
        ...FONTS.p_14px_text,
    },

    time__container: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: '100%',
        marginTop: 10,
        marginBottom: 20,
    },
    cafeImage: {
        height: 150,
        width: '100%',
        alignSelf: 'center',
        borderRadius: 8,
    },
    content__icon: {
        width: 32,
        height: 32,

        marginBottom: 20,
    },

    content__subheading: {
        marginBottom: 2,

        fontSize: 16,
        fontWeight: '600',
        color: '#ccc',
    },

    content__heading: {
        fontSize: 24,
        fontWeight: '600',
        color: '#333',
    },

    content__description: {
        paddingTop: 10,
        paddingBottom: 10,

        fontSize: 15,
        fontWeight: '200',
        lineHeight: 22,
        color: '#666',
    },

    content__input: {
        paddingVertical: 15,
        marginBottom: 20,
        width: '100%',
        borderWidth: 1,
        borderColor: 'transparent',
        borderBottomColor: '#cdcdcd',
        borderRadius: 6,
    },

    content__button: {
        paddingVertical: 15,

        width: '100%',

        backgroundColor: '#333',
        borderRadius: 6,
    },

    content__buttonText: {
        color: '#fff',
        fontSize: 15,
        fontWeight: '600',
        textAlign: 'center',
    },
});
