import React from 'react';
import Dish from '../../assets/icons/38x38/dish.svg';
import DeleteBtn from '../../assets/icons/38x38/delete_btn.svg';

const flags = {
    Dish: Dish,
    DeleteBtn: DeleteBtn,
};

export default function IconCustom38(props) {
    const IconFlag = flags[props.name];
    const size = props.size;
    return (
        <IconFlag
            width={size}
            height={size}
            style={props.style ? props.style : null}
        />
    );
}
