import React from 'react';
import ArrowDown from '../../assets/icons/20x20/arrow_down.svg';
import ArrowRight from '../../assets/icons/20x20/arrow_right.svg';
import ArrowLeft from '../../assets/icons/20x20/arrow_left.svg';
import ArrowUp from '../../assets/icons/20x20/arrow_up.svg';
import User from '../../assets/icons/20x20/back.svg';
import Bell from '../../assets/icons/20x20/bell.svg';
import Building from '../../assets/icons/20x20/building.svg';
import Burger from '../../assets/icons/20x20/burger.svg';
import Calendar from '../../assets/icons/20x20/calendar.svg';
import Check from '../../assets/icons/20x20/check.svg';
import Clock from '../../assets/icons/20x20/clock.svg';
import Close from '../../assets/icons/20x20/close.svg';
import Edit from '../../assets/icons/20x20/edit.svg';
import Globe from '../../assets/icons/20x20/globe.svg';
import HeartFill from '../../assets/icons/20x20/heart_fill.svg';
import Heart from '../../assets/icons/20x20/heart.svg';
import Load from '../../assets/icons/20x20/load.svg';
import Lock from '../../assets/icons/20x20/lock.svg';
import Logout from '../../assets/icons/20x20/logout.svg';
import Next from '../../assets/icons/20x20/next.svg';
import Orders from '../../assets/icons/20x20/orders.svg';
import PhoneCall from '../../assets/icons/20x20/phone-call.svg';
import Pin from '../../assets/icons/20x20/pin.svg';
import Plus from '../../assets/icons/20x20/plus.svg';
import Prev from '../../assets/icons/20x20/prev.svg';
import Search from '../../assets/icons/20x20/search.svg';
import Settings from '../../assets/icons/20x20/settings.svg';
import Sort from '../../assets/icons/20x20/sort.svg';
import Star from '../../assets/icons/20x20/star.svg';
import Support from '../../assets/icons/20x20/support.svg';
import UserIcon from '../../assets/icons/20x20/user.svg';
import CartFilled from '../../assets/icons/28x28/cart_filled.svg';
import CartOutline from '../../assets/icons/20x20/cartoutline.svg';


const flags = {
    ArrowDown: ArrowDown,
    ArrowRight: ArrowRight,
    ArrowLeft: ArrowLeft,
    ArrowUp: ArrowUp,
    User: User,
    Bell: Bell,
    Building: Building,
    Burger: Burger,
    Calendar: Calendar,
    Check: Check,
    Clock: Clock,
    Close: Close,
    Edit: Edit,
    Globe: Globe,
    HeartFill: HeartFill,
    Heart: Heart,
    Load: Load,
    Lock: Lock,
    Logout: Logout,
    Next: Next,
    Orders: Orders,
    PhoneCall: PhoneCall,
    Pin: Pin,
    Plus: Plus,
    Prev: Prev,
    Search: Search,
    Settings: Settings,
    Sort: Sort,
    Star: Star,
    Support: Support,
    UserIcon: UserIcon,
    CartFilled: CartFilled,
    CartOutline: CartOutline,
};

export default function IconCustom(props) {
    const IconFlag = flags[props.name];
    const size = props.size;
    return (
        <IconFlag
            width={size}
            height={size}
            style={props.style ? props.style : null}
        />
    );
}
