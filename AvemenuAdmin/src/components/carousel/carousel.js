import React, { useState } from 'react';
import { Platform, StyleSheet, View } from 'react-native';
import Carousel, {
    Pagination,
    ParallaxImage
} from 'react-native-snap-carousel';
import { DEVICE_HEIGHT, DEVICE_WIDTH } from '../../constants/size';
import { COLORS } from '../../constants/colors';
import { getImage } from '../../utils/images';

export default function CarouselCafe(props) {
    const [activeSlide, setActiveSlide] = useState(0);
    return (
        <View style={styles.container}>
            <Carousel
                sliderWidth={DEVICE_WIDTH - 10}
                sliderHeight={DEVICE_WIDTH}
                itemWidth={DEVICE_WIDTH - 10}
                data={props.images}
                renderItem={_renderItem}
                hasParallaxImages={true}
                inactiveSlideScale={2}
                inactiveSlideOpacity={2}
                loop
                useNativeDriver
                autoplay={true}
                parallaxFactor={1}
                autoplayDelay={6000}
                autoplayInterval={6000}
                onSnapToItem={index => setActiveSlide(index)}
            />
            {pagination()}
        </View>
    );

    function _renderItem({ item, index }, parallaxProps) {
        return (
            <View key={index} style={styles.item}>
                <ParallaxImage
                    source={{
                        uri: getImage(item, 700, 300),
                    }}
                    containerStyle={styles.imageContainer}
                    style={styles.image}
                    parallaxFactor={0.4}
                    {...parallaxProps}
                />
            </View>
        );
    }

    function pagination() {
        return (
            <View
                style={{ alignItems: 'center', marginTop: -50, paddingTop: 0 }}
            >
                <Pagination
                    dotsLength={props.images.length}
                    activeDotIndex={activeSlide}
                    containerStyle={{ width: 1, height: 1 }}
                    dotStyle={{
                        width: 10,
                        height: 10,
                        borderRadius: 5,
                        marginHorizontal: 8,
                        backgroundColor: 'rgba(255, 255, 255, 0.92)',
                    }}
                    inactiveDotStyle={
                        {
                            // Define styles for inactive dots here
                        }
                    }
                    inactiveDotOpacity={0.4}
                    inactiveDotScale={0.6}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
    },
    image: {
        justifyContent: 'center',
        resizeMode: 'cover',
        width: '100%',
    },
    // image: {
    //     ...StyleSheet.absoluteFillObject,
    //     resizeMode: 'cover',
    // },
    titleContainer: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        height: '30%',
    },
    title: {
        color: COLORS.white,
        width: '80%',
        //...STYLES.Fonts.BigBoldTitle
    },
    item: {
        width: DEVICE_WIDTH,
        height: DEVICE_HEIGHT / 4,
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
        //backgroundColor: 'white',
        borderRadius: 16,
        marginHorizontal: 10,
        //alignSelf: 'center',
    },
});
