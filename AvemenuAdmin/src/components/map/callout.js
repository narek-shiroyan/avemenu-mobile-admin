import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList } from 'react-native';
import { DEVICE_WIDTH } from '../../constants/size';


const CARD_HEIGHT = 220;
const CARD_WIDTH = DEVICE_WIDTH * 0.8;


export default function Callout({ index, marker }) {
    return (
        <View style={styles.card} key={index}>
            <Image
                source={marker.image}
                style={styles.cardImage}
                resizeMode="cover"
            />
            <View style={styles.textContent}>
                <Text numberOfLines={1} style={styles.cardtitle}>
                    {marker.title}
                </Text>
                <Text numberOfLines={1} style={styles.cardDescription}>
                    {marker.description}
                </Text>
                <View style={styles.button}>
                    <TouchableOpacity onPress={() => {}} style={styles.signIn}>
                        <Text style={styles.textSign}>Order Now</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <FlatList
                horizontal
                pagingEnabled
                scrollEventThrottle={1}
                showsHorizontalScrollIndicator={false}
                snapToInterval
            />
        </View>
    );
}

const styles = StyleSheet.create({
    card: {
        // padding: 10,
        elevation: 2,
        backgroundColor: '#FFF',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        marginHorizontal: 10,
        shadowColor: '#000',
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: { x: 2, y: -2 },
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        overflow: 'hidden',
    },
    cardImage: {
        flex: 3,
        width: '100%',
        height: '100%',
        alignSelf: 'center',
    },
    cardtitle: {
        fontSize: 12,
        // marginTop: 5,
        fontWeight: 'bold',
    },
    cardDescription: {
        fontSize: 12,
        color: '#444',
    },
    textSign: {
        fontSize: 14,
        fontWeight: 'bold',
    },
});
