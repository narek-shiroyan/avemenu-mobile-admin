/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { COLORS } from '../../constants/colors';
import { FONTS } from '../../constants/fonts';
import IconButton from '../buttons/IconButton';

export default function BottomCardPanel(props) {
    const { cb } = props;

    return (
        <TouchableOpacity onPress={cb} style={styles.container}>
            <Text style={styles.text__price}>
                Перейти в корзину
            </Text>
            <View
                style={{
                    alignSelf: 'center',
                    marginRight: 20,
                    flexDirection: 'row',
                }}
            >
                <IconButton
                    name="CartFilled"
                    size={28}
                    cb={() => { }}
                    customStyles={{ width: 40, height: 40 }}
                />
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '92%',
        backgroundColor: COLORS.coral,
        position: 'absolute',
        bottom: 50,
        alignSelf: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        height: 60,
        borderRadius: 16,
    },
    text__price: {
        ...FONTS.btnCaption.white15px,
        alignSelf: 'center',
        marginLeft: 20,
    },
});
