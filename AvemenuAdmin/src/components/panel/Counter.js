import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { COLORS } from '../../constants/colors';
import { FONTS } from '../../constants/fonts';
import Minus from '../../assets/icons/minus_white.svg';
import Plus from '../../assets/icons/plus_white.svg';
import MinusPurple from '../../assets/icons/minus_purple.svg';
import PlusPurple from '../../assets/icons/plus_purple.svg';

export default function Counter(props) {
    const {
        clickMinus,
        clickPlus,
        counter,
        customStyles,
        customNumberStyle,
        purple,
    } = props;
    return (
        <View style={[styles.container, customStyles ? customStyles : null]}>
            <TouchableOpacity style={styles.btn} onPress={clickMinus}>
                {!purple ? <Minus /> : <MinusPurple />}
            </TouchableOpacity>
            <Text
                style={[
                    styles.text,
                    customNumberStyle ? customNumberStyle : null,
                ]}
            >
                {counter}
            </Text>
            <TouchableOpacity style={styles.btn} onPress={clickPlus}>
                {!purple ? <Plus /> : <PlusPurple />}
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: 100,
        height: 40,
        borderRadius: 8,
        borderColor: COLORS.white,
        borderWidth: 2,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    text: {
        ...FONTS.btnCaption.h4_16px_white,
        alignSelf: 'center',
    },
    btn: {
        alignSelf: 'center',
        height: '100%',
        justifyContent: 'center',
    },
});
