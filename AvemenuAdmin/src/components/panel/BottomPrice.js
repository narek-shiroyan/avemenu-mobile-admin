/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { COLORS } from '../../constants/colors';
import { FONTS } from '../../constants/fonts';
import { CURRENCY } from '../../constants/currency';
import IconButton from '../buttons/IconButton';
import Counter from './Counter';

export default function BottomPrice(props) {
    const { food, cb } = props;
    const [counter, setCounter] = useState(1);
    
    return (
        <View style={styles.container}>
            <Text style={styles.text__price}>
                {food.price * counter} {CURRENCY.ru}
            </Text>
            <View
                style={{
                    alignSelf: 'center',
                    marginRight: 20,
                    flexDirection: 'row',
                }}
            >
                <View style={{ marginRight: 10 }}>
                    <Counter
                        counter={counter}
                        clickMinus={() =>
                            counter > 1 ? setCounter(counter - 1) : null
                        }
                        clickPlus={() => setCounter(counter + 1)}
                    />
                </View>
                <IconButton
                    name="CartFilled"
                    size={28}
                    cb={() => cb({ ...food, counter })}
                    customStyles={{ width: 40, height: 40 }}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '92%',
        backgroundColor: COLORS.coral,
        position: 'absolute',
        bottom: 50,
        alignSelf: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        height: 60,
        borderRadius: 16,
    },
    text__price: {
        ...FONTS.btnCaption.white15px,
        alignSelf: 'center',
        marginLeft: 20,
    },
});
