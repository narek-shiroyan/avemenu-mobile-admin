import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { COLORS } from '../../constants/colors';
import { FONTS } from '../../constants/fonts';

export default function TopWhiteIcon({ title, iconName, cb }) {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>{title}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLORS.white,
        height: 48,
        width: '85%',
        position: 'absolute',
        alignSelf: 'center',
        justifyContent: 'center',
        top: '5%',
        borderRadius: 8,
    },
    text: {
        ...FONTS.btnCaption.h4_16px_dark,
        textAlign: 'center',
    },
});
