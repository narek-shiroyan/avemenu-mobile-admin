import messaging from '@react-native-firebase/messaging';
//import { setPushToken } from 'src/utils/storage';
import { setPushToken } from './src/utils/storage';

// Получение токена
// requestUserPermission();
// const unsubscribe = messaging().onMessage(async remoteMessage => {
//     Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
// });
// return unsubscribe;

export const requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
        authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
        authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    if (enabled) {
        await getFcmToken() //<---- Add this
    }
}

async function getFcmToken() {

    const fcmToken = await messaging().getToken();

    if (fcmToken) {
        // console.log('fcm token', fcmToken);
        // Записываем в storage
        //await setPushToken(fcmToken);
        // Отправляем на сервер
        //await sendPushToken(fcmToken);
        console.log('FCM TOKEN', fcmToken);
        setPushToken(fcmToken);
    } else {
        console.log("Failed", "No token received");
    }
}
