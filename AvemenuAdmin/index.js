/**
 * @format
 */

import messaging from '@react-native-firebase/messaging';
import { AppRegistry } from 'react-native';
import 'react-native-gesture-handler';
import 'react-native-reanimated';
import App from './App';
import { name as appName } from './app.json';
import { requestUserPermission } from './fcm-connect';

// Информация которая прилетит с пуш
let pushContent = null;

requestUserPermission();
messaging().setBackgroundMessageHandler(async remoteMessage => {
    console.log('Message handled in the background!', remoteMessage);
});

AppRegistry.registerComponent(appName, () => App);
